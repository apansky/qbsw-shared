import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { QbswGridModule } from 'dist/qbsw-grid';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    QbswGridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
