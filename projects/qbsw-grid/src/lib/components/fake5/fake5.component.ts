import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-fake5',
  templateUrl: './fake5.component.html',
  styleUrls: ['./fake5.component.css']
})
export class Fake5Component implements OnInit {

  @Input() message: string;

  constructor() { }

  ngOnInit() {
  }

}
