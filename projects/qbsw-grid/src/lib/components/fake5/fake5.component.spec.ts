import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Fake5Component } from './fake5.component';

describe('Fake5Component', () => {
  let component: Fake5Component;
  let fixture: ComponentFixture<Fake5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fake5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fake5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
