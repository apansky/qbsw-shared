import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import * as QbswGridActions from './../../qbsw-grid.actions';
import * as fromQbswGrid from './../../qbsw-grid.reducer';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'lib-fake1',
  templateUrl: './fake1.component.html',
  styleUrls: ['./fake1.component.css']
})
export class Fake1Component implements OnInit {

  @Input() message: string;
  fakeForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<fromQbswGrid.State>
  ) { }

  ngOnInit() {
    this.fakeForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
    this.fakeForm.statusChanges
      .pipe(distinctUntilChanged())
      .subscribe((data) => this.store.dispatch(QbswGridActions.setComponentValidity({
        component: 'fake1',
        validity: data === 'VALID'
      })));
  }

}
