import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-fake2',
  templateUrl: './fake2.component.html',
  styleUrls: ['./fake2.component.css']
})
export class Fake2Component implements OnInit {

  @Input() message: string;

  constructor() { }

  ngOnInit() {
  }

}
