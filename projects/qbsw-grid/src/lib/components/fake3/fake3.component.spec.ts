import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Fake3Component } from './fake3.component';

describe('Fake3Component', () => {
  let component: Fake3Component;
  let fixture: ComponentFixture<Fake3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fake3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fake3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
