import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-fake4',
  templateUrl: './fake4.component.html',
  styleUrls: ['./fake4.component.css']
})
export class Fake4Component implements OnInit {

  @Input() message: string;

  constructor() { }

  ngOnInit() {
  }

}
