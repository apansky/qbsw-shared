import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Fake4Component } from './fake4.component';

describe('Fake4Component', () => {
  let component: Fake4Component;
  let fixture: ComponentFixture<Fake4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fake4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fake4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
