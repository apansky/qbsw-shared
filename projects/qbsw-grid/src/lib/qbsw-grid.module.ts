import { NgModule } from '@angular/core';
import { QbswGridComponent } from './qbsw-grid.component';
import { GridsterModule } from 'angular-gridster2';
import { CommonModule } from '@angular/common';
import { QbswGridItemDirective } from './directives/qbsw-grid-item.directive';
import { QbswGridService } from './qbsw-grid.service';
import * as fromQbswGrid from './qbsw-grid.reducer';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ReactiveFormsModule } from '@angular/forms';

import { Fake1Component } from './components/fake1/fake1.component';
import { Fake2Component } from './components/fake2/fake2.component';
import { Fake3Component } from './components/fake3/fake3.component';
import { Fake4Component } from './components/fake4/fake4.component';
import { Fake5Component } from './components/fake5/fake5.component';

@NgModule({
  declarations: [
    QbswGridComponent,
    QbswGridItemDirective,
    Fake1Component,
    Fake2Component,
    Fake3Component,
    Fake4Component,
    Fake5Component
  ],
  entryComponents: [
    QbswGridComponent,
    Fake1Component,
    Fake2Component,
    Fake3Component,
    Fake4Component,
    Fake5Component
  ],
  imports: [
    CommonModule,
    GridsterModule,
    StoreModule.forRoot({
      'qbsw-grid': fromQbswGrid.reducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    }),
    ReactiveFormsModule
  ],
  exports: [
    QbswGridComponent
  ],
  providers: [
    QbswGridService
  ]
})
export class QbswGridModule { }
