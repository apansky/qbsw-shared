import {
  Directive,
  Input,
  OnChanges,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef
} from '@angular/core';

import { Fake1Component } from './../components/fake1/fake1.component';
import { Fake2Component } from './../components/fake2/fake2.component';
import { Fake3Component } from './../components/fake3/fake3.component';
import { Fake4Component } from './../components/fake4/fake4.component';
import { Fake5Component } from './../components/fake5/fake5.component';

const components = {
  fake1: Fake1Component,
  fake2: Fake2Component,
  fake3: Fake3Component,
  fake4: Fake4Component,
  fake5: Fake5Component
};

@Directive({
  selector: '[libQbswGridItem]'
})
export class QbswGridItemDirective implements OnChanges {

  @Input() componentRef: string;
  @Input() message: string;
  component: ComponentRef<ComponentCollection>;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {}

  ngOnChanges() {
    const component = components[this.componentRef];

    if (component) {
      const factory = this.componentFactoryResolver.resolveComponentFactory<ComponentCollection>(component);
      this.component = this.viewContainerRef.createComponent(factory);
      this.component.instance.message = this.message;
    }
  }
}

export type ComponentCollection
  = Fake1Component
  | Fake2Component
  | Fake3Component
  | Fake4Component
  | Fake5Component;
