import { Action, createReducer, createFeatureSelector, on, createSelector } from '@ngrx/store';
import * as QbswGridActions from './qbsw-grid.actions';

export const qbswGridFeatureKey = 'qbsw-grid';
export const initialState: State = {
  fake1: {
    formValid: false
  },
  fake2: {
    formValid: true
  },
  fake3: {
    formValid: false
  },
  fake4: {
    formValid: true
  },
  fake5: {
    formValid: true
  }
};

const qbswGridReducer = createReducer(
  initialState,
  on(QbswGridActions.setComponentValidity, (state, action) => {
    const newState = { ...state };
    newState[action.component].formValid = action.validity;
    return newState;
  })
);

export function reducer(state: State | undefined, action: Action) {
  return qbswGridReducer(state, action);
}

export const selectFeature = createFeatureSelector<State>(qbswGridFeatureKey);
export const selectAreAllValid = createSelector(selectFeature, (state: State) => Object.values(state).every(c => c.formValid));

export interface State {
  fake1: SomeComponentData;
  fake2: SomeComponentData;
  fake3: SomeComponentData;
  fake4: SomeComponentData;
  fake5: SomeComponentData;
}

export interface SomeComponentData {
  formValid: boolean;
}
