import { TestBed } from '@angular/core/testing';

import { QbswGridService } from './qbsw-grid.service';

describe('QbswGridService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QbswGridService = TestBed.get(QbswGridService);
    expect(service).toBeTruthy();
  });
});
