import { createAction, props } from '@ngrx/store';

export const setComponentValidity = createAction(
  '[Grid] Set Component Validity',
  props<{ component: string, validity: boolean }>()
);
