export interface GridItem {
  cols: number;
  componentId: string;
  id: string;
  rows: number;
  x: number;
  y: number;
}
