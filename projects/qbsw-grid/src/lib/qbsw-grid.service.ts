import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { layout } from './../fixtures/layout';
import { GridItem } from './qbsw-grid.interfaces';

@Injectable()
export class QbswGridService {

  fetchLayout() {
    return new Observable((observer: Observer<GridItem[]>) => {
      observer.next(layout);
    });
  }

}
