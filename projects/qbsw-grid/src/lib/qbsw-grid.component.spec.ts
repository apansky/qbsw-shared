import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QbswGridComponent } from './qbsw-grid.component';

describe('QbswGridComponent', () => {
  let component: QbswGridComponent;
  let fixture: ComponentFixture<QbswGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QbswGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QbswGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
