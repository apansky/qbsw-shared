import { Component } from '@angular/core';
import { CompactType, DisplayGrid, GridsterConfig, GridType } from 'angular-gridster2';
import { QbswGridService } from './qbsw-grid.service';
import { GridItem } from './qbsw-grid.interfaces';
import { Store } from '@ngrx/store';
import * as fromQbswGrid from './qbsw-grid.reducer';

@Component({
  selector: 'lib-qbsw-grid',
  templateUrl: './qbsw-grid.component.html',
  styleUrls: ['./qbsw-grid.component.scss']
})
export class QbswGridComponent {

  areAllValid: boolean;
  layout: GridItem[];
  options: GridsterConfig = {
    itemChangeCallback: (item: GridItem) => this.gridsterItemChange(item),
    gridType: GridType.Fit,
    compactType: CompactType.None,
    margin: 10,
    outerMargin: true,
    outerMarginTop: null,
    outerMarginRight: null,
    outerMarginBottom: null,
    outerMarginLeft: null,
    useTransformPositioning: true,
    mobileBreakpoint: 640,
    minCols: 1,
    maxCols: 10,
    minRows: 1,
    maxRows: 15,
    maxItemCols: 100,
    minItemCols: 1,
    maxItemRows: 100,
    minItemRows: 1,
    maxItemArea: 2500,
    minItemArea: 1,
    defaultItemCols: 1,
    defaultItemRows: 1,
    fixedColWidth: 105,
    fixedRowHeight: 105,
    keepFixedHeightInMobile: false,
    keepFixedWidthInMobile: false,
    scrollSensitivity: 10,
    scrollSpeed: 20,
    enableEmptyCellClick: false,
    enableEmptyCellContextMenu: false,
    enableEmptyCellDrop: false,
    enableEmptyCellDrag: false,
    emptyCellDragMaxCols: 50,
    emptyCellDragMaxRows: 50,
    ignoreMarginInRow: false,
    draggable: {
      enabled: true,
    },
    resizable: {
      enabled: true,
    },
    swap: false,
    pushItems: true,
    disablePushOnDrag: false,
    disablePushOnResize: false,
    pushDirections: {north: true, east: true, south: true, west: true},
    pushResizeItems: false,
    displayGrid: DisplayGrid.Always,
    disableWindowResize: false,
    disableWarnings: false,
    scrollToNewItems: false
  };

  constructor(
    private readonly qbswGridService: QbswGridService,
    private store: Store<fromQbswGrid.State>
  ) {
    this.qbswGridService.fetchLayout()
      .subscribe((layout) => this.layout = layout);
    this.store.select(fromQbswGrid.selectAreAllValid)
      .subscribe((areAllValid) => this.areAllValid = areAllValid);
  }

  get layoutPreview() {
    return this.layout.map((item) => ({
      cols: item.cols,
      row: item.rows,
      x: item.x,
      y: item.y
    }));
  }

  gridsterItemChange(item: GridItem) {
    console.log(item);
  }

}
