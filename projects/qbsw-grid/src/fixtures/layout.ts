export const layout = [
  {
    cols: 10,
    id: '8742229e-1f9a-4e9d-a479-01d47ff776ad',
    rows: 5,
    x: 0,
    y: 0,
    componentId: 'fake1'
  },
  {
    cols: 3,
    id: '6d05bd38-24a6-42c5-8755-b3c557763131',
    rows: 5,
    x: 0,
    y: 5,
    componentId: 'fake2'
  },
  {
    cols: 7,
    id: '3b48c86c-eed0-41d4-b865-2512567dc5f1',
    rows: 5,
    x: 3,
    y: 5,
    componentId: 'fake3'
  },
  {
    cols: 7,
    id: 'f99fde0b-ce1d-4cf2-8b1a-fee058b31d87',
    rows: 5,
    x: 0,
    y: 10,
    componentId: 'fake4'
  },
  {
    cols: 3,
    id: '78209d24-0900-4189-858f-4a230731d15c',
    rows: 5,
    x: 7,
    y: 10,
    componentId: 'fake5'
  }
];
